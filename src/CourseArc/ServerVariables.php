<?php

namespace CourseArc;

class ServerVariables {

  public function initialize() {
    if ( !isset($_SERVER['HTTP_HOST']) ) $_SERVER['HTTP_HOST'] = 'cli';
    if ( !isset($_SERVER['SERVER_NAME']) ) $_SERVER['SERVER_NAME'] = 'cli';
    if ( !isset($_SERVER['REQUEST_URI']) ) $_SERVER['REQUEST_URI'] = '';
    if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
      $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    if ( isset($_SERVER['HTTP_X_FORWARDED_PORT']) && !empty($_SERVER['HTTP_X_FORWARDED_PORT']) ) {
      $_SERVER['REMOTE_PORT'] = $_SERVER['HTTP_X_FORWARDED_PORT'];
    }
    if ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) {
      $_SERVER['HTTPS']        = 'on';
      $_SERVER['SERVER_PORT']  = 443;
    }
  }

}
