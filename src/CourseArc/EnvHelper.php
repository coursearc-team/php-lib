<?php

namespace CourseArc;

class EnvHelper {

  public function initialize($envDirectory, $vendorDirectory = null) {
    if ($vendorDirectory == null) $vendorDirectory = $envDirectory . '/vendor';
    if ( file_exists( $envDirectory . "/.env" ) ) {
      require_once $vendorDirectory . "/autoload.php";
      $dotenv = new \Dotenv\Dotenv($envDirectory);
      $dotenv->load();
    }
    foreach ($_ENV as $key => $value) {
      if ( ! defined( $key ) ) define( $key, $this->get($key) );
    }
  }

  public function get($name, $default = null) {
    $env = getenv($name);
    if ( $env === false ) return $default;
    if ( strtolower(trim($env)) == "false" || strtolower(trim($env)) == "true" ) return filter_var(strtolower(trim($env)), FILTER_VALIDATE_BOOLEAN);
    if ( is_numeric($env) ) return ($env - 0);
    return $env;
  }

}
