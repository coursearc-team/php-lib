<?php

use PHPUnit\Framework\TestCase;

class EnvHelperTest extends TestCase {

  public function testInitialize() {

    $envHelper = new \CourseArc\EnvHelper();
    $envHelper->initialize(__DIR__ . '/../assets', __DIR__ . '/../../vendor');

    $this->assertEquals($envHelper->get('ONE'), 1);
    $this->assertEquals($envHelper->get('VALUE'), 'testvalue');
    $this->assertEquals($envHelper->get('BOOLEAN_TRUE'), true);
    $this->assertEquals($envHelper->get('BOOLEAN_FALSE'), false);

    $this->assertEquals(ONE, 1);
    $this->assertEquals(VALUE, 'testvalue');
    $this->assertEquals(BOOLEAN_TRUE, true);
    $this->assertEquals(BOOLEAN_FALSE, false);

  }

}
