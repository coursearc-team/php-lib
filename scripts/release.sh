#!/bin/bash

type="$1"

project_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $project_dir
if ! git status | grep "nothing to commit"; then
  echo "You have pending changes, you should commit first before releasing"
  exit 1
fi

last_tag=$(git for-each-ref --sort=-taggerdate --format '%(refname) %(taggerdate)' refs/tags | head -1 | awk -F '\/' '{print $3}')
tag_parts=(${last_tag//./ })
major_version=${tag_parts[0]}
minor_version=${tag_parts[1]}
patch_version=${tag_parts[2]}
if [[ "$type" == "major" ]]; then
  major_version=$(( $major_version + 1 ))
  minor_version="0"
  patch_version="0"
elif [[ "$type" == "minor" ]]; then
  minor_version=$(( $minor_version + 1 ))
  patch_version="0"
elif [[ "$type" == "patch" ]]; then
  patch_version=$(( $patch_version + 1 ))
else
  echo "Unknown release type: $type, must be on of: 'major', 'minor', or 'patch'"
  exit 1
fi

git checkout master
git rebase develop

git tag "${major_version}.${minor_version}.${patch_version}"

git checkout develop
git rebase master

git push origin --all
git push origin --tags

echo "NEW VERSION released to packagist.org: ${major_version}.${minor_version}.${patch_version}"
