# CourseArc PHP library

This is a place to put some PHP classes that will be shared across projects. It's shared on packagist.org as a public project, so you should **only include things in this project that wouldn't be considered sensitive or intellectual property of CourseArc**.

## The Packagist Project

[coursearc/php-lib](https://packagist.org/packages/coursearc/php-lib)

### How to develop and release

1. Make whatever changes to `/src`
2. Update or add tests in `/tests`
3. Run tests to make sure everything is working: `composer run-script test`
4. Release your changes: `composer run-script release`
5. Make sure to update any other projects that need to reference the new version
